#include "rowset.h"

#include "row.h"

#include <iostream>
#include <vector>

namespace shdb {

Rowset::Rowset()
{}

Rowset::Rowset(std::shared_ptr<Schema> schema) : schema(std::move(schema))
{}

Rowset::Rowset(Rowset &&other)
{
    schema = std::move(other.schema);
    rows = std::move(other.rows);
}

Row *Rowset::allocate()
{
    auto *row = new Row(schema->size());
    rows.push_back(row);
    return row;
}

void Rowset::sort_rows(int (*comparer)(const Row *lhs, const Row *rhs))
{
    std::sort(rows.begin(), rows.end(), [&](auto *lhs, auto *rhs) { return comparer(lhs, rhs) < 0; });
}

Rowset::~Rowset()
{
    for (auto *row : rows) {
        delete row;
    }
}


Hashset::Hashset()
{}

Hashset::Hashset(std::shared_ptr<Schema> schema) : schema(std::move(schema))
{}

Hashset::Hashset(Hashset &&other) : schema(std::move(other.schema)), key_to_rowset(std::move(other.key_to_rowset))
{}

Rowset *Hashset::find(Row *key)
{
    auto it = key_to_rowset.find(*key);
    return it == key_to_rowset.end() ? nullptr : &it->second;
}

Rowset *Hashset::find_or_create(Row *key)
{
    auto it = key_to_rowset.find(*key);
    if (it == key_to_rowset.end()) {
        it = key_to_rowset.insert(std::make_pair(*key, Rowset(schema))).first;
    }
    return &it->second;
}


Groupset::Groupset()
{}

Groupset::Groupset(std::shared_ptr<Schema> key_schema, size_t state_size)
    : keys(std::move(key_schema)), state_size(state_size)
{}

Groupset::Groupset(Groupset &&other)
    : keys(std::move(other.keys))
    , state_size(other.state_size)
    , states(std::move(other.states))
    , key_to_index(std::move(other.key_to_index))
{}

Groupset::~Groupset()
{
    for (auto *state : states) {
        delete[] state;
    }
}

void *Groupset::find_state(Row *key)
{
    auto it = key_to_index.find(*key);
    return it == key_to_index.end() ? nullptr : states[it->second];
}

void *Groupset::create_state(Row *key)
{
    states.push_back(new char[state_size]);
    *(keys.allocate()) = *key;
    key_to_index[*key] = states.size() - 1;
    return states.back();
}

}    // namespace shdb
