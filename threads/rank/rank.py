#!/usr/bin/python

import argparse
import collections
import csv
import glob
import itertools
import math
import re
import sys

def readStdoutLog(filename):
  f = open(filename)
  r = re.compile('Child process time ([0-9]+) ms\n')
  total = 0
  lines = 0
  for line in f.readlines():
    g = re.match(r, line)
    if not g:
      raise RuntimeError()
    lines += 1
    total += int(g.group(1))

  if lines != 1:
    sys.stderr.write("%d data lines found in %s\n" % (lines, filename))
    raise RuntimeError()

  return total

def durationSortKey(y):
  if type(y) == int:
    return y
  else:
    return 100000

def tableSortKey(row, ref):
  for r in row[1]:
    if r != 'fail':
      assert(type(r) == float)

  n = len(row[1])
  assert(n == len(ref[1]))

  failures = sum([1 if r == 'fail' else 0 for r in row[1]])
  duration = sum([1.0 * r/rref if type(r) == float else 0 for (r, rref) in zip(row[1], ref[1])]) / n

  return (failures, duration)

def position(username, table):
  for i in range(len(table)):
    if table[i][0] == username:
      return i
  assert False

def durationMedian(y):
  if len(y) % 2 == 1:
    return y[len(y) / 2]
  else:
    index = int(len(y) / 2)
    if type(y[index - 1]) == str or type(y[index]) == str:
      return y[index]
    return (y[index - 1] + y[index]) / 2

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('--output_raw', type=str)
  parser.add_argument('--output', type=str)
  parser.add_argument('--reference', type=str)
  args = parser.parse_args()

  pattern = './logs/*.[1-4].[1-6].*'
  regexp = re.compile('\/([a-zA-Z0-9_\-]+)\.([1-4])\.([1-6])\.([a-z]+)')

  files = sorted(list(glob.glob(pattern)))
  runs = collections.defaultdict(dict)
  num_runs = 0

  for filename in files:
    g = re.search(regexp, filename)
    if not g:
      continue
    (username, runid, testid, filetype) = (g.group(1), int(g.group(2)), int(g.group(3)), g.group(4))

    num_runs = max(num_runs, runid)
    if filetype == 'skip' or filetype == 'fail':
      runs[(username, testid)][runid] = 'fail'
    elif filetype == 'stdout':
      if runs[(username, testid)].get(runid) != 'fail':
        total_ms = readStdoutLog(filename)
        runs[(username, testid)][runid] = total_ms

  raw = {}
  for k, v in runs.items():
    raw[k] = sorted(v.values(), key = durationSortKey)

  r = collections.defaultdict(dict)
  for k, v in raw.items():
    r[k[0]][k[1]] = durationMedian(v)

  table = []
  ref = None
  for k, v in r.items():
    t = (k, [y[1] for y in sorted(v.items(), key = lambda y: y[0])])
    if k == args.reference:
      ref = t
    table.append(t)

  table.sort(key=lambda row: tableSortKey(row, ref))

  num_tests = len(table[0][1])
  for r in table:
    assert(len(r[1]) == num_tests)

  if args.output_raw:
    out = csv.writer(open(args.output_raw, 'w'))

    t = collections.defaultdict(dict)
    for (username, testid), rr in runs.items():
      for runid, v in rr.items():
        t[username][(testid, runid)] = v

    raw_table = []
    for username, d in t.items():
      raw_table.append((
        username,
        [v for (_, v) in sorted(d.items(), key = lambda y: y[0])],
      ))

    raw_table.sort(key=lambda y: position(y[0], table))
    header = ['username'] + [
      'Test %d run %d ms' % (run, test) for (run, test) in itertools.product(range(1, num_tests + 1), range(1, num_runs + 1))
    ]

    out.writerow(header)
    for row in raw_table:
      out.writerow([row[0]] + row[1])

  if args.output:
    out = csv.writer(open(args.output, 'w'))
    out.writerow(['username'] + ['test %d (median)' % i for i in range(1, num_tests + 1)] + ['num failures', 'avg(duration_ms/reference duration_ms)'])
    for row in table:
      (numFailures, totalDuration) = tableSortKey(row, ref)
      outrow = [row[0]] + row[1] + [numFailures, totalDuration]
      out.writerow(outrow)


if __name__ == '__main__':
  main()
