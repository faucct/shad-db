#!/bin/bash

set -e
set -x

# 2022
usernames=(
  "gepardo"
  "bkuchin"
  "erzyadev"
  "kononovk"
  "zeronsix"
  "nsvasilev"
  "faucct"
  "svlads"
  "d4rk"
  "kitaisreal"
  "Fedya001"
  "AlexJokel"
  "yesenarman"
  "gripon"
  "Kifye"
  "dkhorkin"
  "mbkkt"
  "pervakovg"
  "Panesher"
  "TEduard"
  "alexeysm"
  "bazarinm"

  "dnorlov"
)

git clone git@gitlab.com:savrus/shad-db.git

for username in ${usernames[@]}
do
  url="git@gitlab.manytask.org:db-spring-2022/${username}.git"
  git clone ${url}
  cd ${username}
  git checkout submits/threads
  cd ..
  echo "Done checking out ${username}"
done
