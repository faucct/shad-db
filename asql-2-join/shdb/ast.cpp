#include "ast.h"

#include <cassert>

namespace shdb {

Ast::Ast(AstType type) : type(type)
{}

String::String(AstType type, std::string value) : Ast(type), value(value)
{}

Number::Number(int value) : Ast(AstType::number), value(value)
{}

BinaryOp::BinaryOp(Opcode op, std::shared_ptr<Ast> lhs, std::shared_ptr<Ast> rhs)
    : Ast(AstType::binary), op(op), lhs(lhs), rhs(rhs)
{}

UnaryOp::UnaryOp(Opcode op, std::shared_ptr<Ast> operand) : Ast(AstType::unary), op(op), operand(operand)
{}

List::List() : Ast(AstType::list)
{}

List::List(std::shared_ptr<Ast> element) : Ast(AstType::list), list{element}
{}

void List::append(std::shared_ptr<Ast> element)
{
    list.push_back(element);
}

Order::Order(std::shared_ptr<Ast> expr, bool desc) : Ast(AstType::order), expr(std::move(expr)), desc(desc)
{}

Select::Select(
    std::shared_ptr<List> list,
    std::vector<std::string> from,
    std::shared_ptr<Ast> where,
    std::shared_ptr<List> order)
    : Ast(AstType::select)
    , list(list)
    , from(std::move(from))
    , where(std::move(where))
    , order(std::move(order))
{}

Insert::Insert(std::string table, std::shared_ptr<List> values)
    : Ast(AstType::insert), table(std::move(table)), values(values)
{}

Create::Create(std::string table, std::shared_ptr<Schema> schema)
    : Ast(AstType::create), table(std::move(table)), schema(std::move(schema))
{}

Drop::Drop(std::string table) : Ast(AstType::drop), table(std::move(table))
{}


std::shared_ptr<Ast> new_name(std::string value)
{
    return std::make_shared<String>(AstType::name, std::move(value));
}

std::shared_ptr<Ast> new_string(std::string value)
{
    return std::make_shared<String>(AstType::string, std::move(value));
}

std::shared_ptr<Ast> new_number(int value)
{
    return std::make_shared<Number>(value);
}

std::shared_ptr<Ast> new_binary(Opcode op, std::shared_ptr<Ast> lhs, std::shared_ptr<Ast> rhs)
{
    return std::make_shared<BinaryOp>(op, lhs, rhs);
}

std::shared_ptr<Ast> new_unary(Opcode op, std::shared_ptr<Ast> operand)
{
    return std::make_shared<UnaryOp>(op, operand);
}

std::shared_ptr<List> new_list()
{
    return std::make_shared<List>();
}

std::shared_ptr<List> new_list(std::shared_ptr<Ast> operand)
{
    return std::make_shared<List>(operand);
}

std::shared_ptr<Ast> new_order(std::shared_ptr<Ast> expr, bool desc)
{
    return std::make_shared<Order>(std::move(expr), desc);
}

std::shared_ptr<Ast> new_select(
    std::shared_ptr<List> list,
    std::vector<std::string> from,
    std::shared_ptr<Ast> where,
    std::shared_ptr<List> order)
{
    return std::make_shared<Select>(list, std::move(from), std::move(where), std::move(order));
}

std::shared_ptr<Ast> new_insert(std::string table, std::shared_ptr<List> values)
{
    return std::make_shared<Insert>(std::move(table), std::move(values));
}

std::shared_ptr<Ast> new_create(std::string table, Schema schema)
{
    return std::make_shared<Create>(std::move(table), std::make_shared<Schema>(std::move(schema)));
}

std::shared_ptr<Ast> new_drop(std::string table)
{
    return std::make_shared<Drop>(std::move(table));
}

std::string to_string(std::shared_ptr<Ast> ast)
{
    if (!ast) {
        return {};
    }

    switch (ast->type) {
    case AstType::name:
        return std::static_pointer_cast<String>(ast)->value;
    case AstType::string:
        return std::string("\"") + std::static_pointer_cast<String>(ast)->value + "\"";
    case AstType::number:
        return std::to_string(std::static_pointer_cast<Number>(ast)->value);
    case AstType::binary: {
        auto op = std::static_pointer_cast<BinaryOp>(ast);
        auto get_op = [&]() -> std::string {
            switch (op->op) {
            case Opcode::plus:
                return "+";
            case Opcode::minus:
                return "-";
            case Opcode::mul:
                return "*";
            case Opcode::div:
                return "/";
            case Opcode::land:
                return "AND";
            case Opcode::lor:
                return "OR";
            case Opcode::eq:
                return "=";
            case Opcode::ne:
                return "<>";
            case Opcode::lt:
                return "<";
            case Opcode::le:
                return "<=";
            case Opcode::gt:
                return ">";
            case Opcode::ge:
                return ">=";
            default:
                assert(0);
            }
        };
        return std::string("(") + to_string(op->lhs) + ") " + get_op() + " (" + to_string(op->rhs) + ")";
    }
    case AstType::unary: {
        auto op = std::static_pointer_cast<UnaryOp>(ast);
        auto get_op = [&]() -> std::string {
            switch (op->op) {
            case Opcode::lnot:
                return "!";
            case Opcode::uminus:
                return "-";
            default:
                assert(0);
            }
        };
        return get_op() + " (" + to_string(op->operand) + ")";
    }
    case AstType::order: {
        auto order = std::static_pointer_cast<Order>(ast);
        return to_string(order->expr) + (order->desc ? " DESC" : "");
    }
    case AstType::list: {
        auto op = std::static_pointer_cast<List>(ast);
        std::string result = to_string(op->list[0]);
        for (size_t index = 1; index < op->list.size(); ++index) {
            result += ", " + to_string(op->list[index]);
        }
        return result;
    }
    case AstType::select: {
        auto op = std::static_pointer_cast<Select>(ast);
        auto result = std::string("SELECT ") + (op->list ? to_string(op->list) : "*");
        if (!op->from.empty()) {
            result += " FROM " + op->from[0];
            for (size_t i = 1; i < op->from.size(); ++i) {
                result += ", " + op->from[i];
            }
        }
        if (op->where) {
            result += " WHERE " + to_string(op->where);
        }
        if (op->order) {
            result += " ORDER BY " + to_string(op->order);
        }
        return result;
    }
    case AstType::insert: {
        auto op = std::static_pointer_cast<Insert>(ast);
        return "INSERT " + op->table + " VALUES (" + to_string(op->values) + ")";
    }
    case AstType::create: {
        auto op = std::static_pointer_cast<Create>(ast);
        auto col_to_string = [&](const auto &column) {
            auto result = column.name + " ";
            switch (column.type) {
            case Type::uint64:
                result += "UINT64";
                break;
            case Type::boolean:
                result += "BOOLEAN";
                break;
            case Type::string:
                result += "STRING";
                break;
            case Type::varchar:
                result += "VARCHAR(" + std::to_string(column.length) + ")";
                break;
            default:
                assert(0);
            }
            return result;
        };
        auto &schema = *op->schema;
        auto result = "CREATE TABLE " + op->table + " (" + col_to_string(schema[0]);
        for (size_t index = 1; index < schema.size(); ++index) {
            result += ", " + col_to_string(schema[index]);
        }
        return result + ")";
    }
    case AstType::drop: {
        auto op = std::static_pointer_cast<Drop>(ast);
        return "DROP TABLE " + op->table;
    }
    default: {
        assert(0);
    }
    }
}

}    // namespace shdb
