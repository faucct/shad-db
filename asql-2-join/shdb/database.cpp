#include "database.h"

#include "fixed.h"
#include "flexible.h"

#include <memory>

namespace shdb {

Database::Database(const std::filesystem::path &path, FrameIndex frame_count)
    : statistics(std::make_shared<Statistics>())
    , store(std::make_shared<Store>(path, frame_count, statistics))
    , catalog(std::make_shared<Catalog>(store))
{}

void Database::create_table(const std::filesystem::path &name, std::shared_ptr<Schema> schema)
{
    catalog->save_table_schema(name, schema);
    store->create_table(name);
}

std::shared_ptr<Table> Database::get_table(const std::filesystem::path &name, std::shared_ptr<Schema> schema)
{
    if (!schema) {
        schema = catalog->find_table_schema(name);
    }
    auto provider = create_page_provider(std::move(schema));
    return store->open_table(name, std::move(provider));
}

bool Database::check_table_exists(const std::filesystem::path &name)
{
    return store->check_table_exists(name);
}

void Database::drop_table(const std::filesystem::path &name)
{
    catalog->forget_table_schema(name);
    store->remove_table(name);
}

std::shared_ptr<Statistics> Database::get_statistics()
{
    return statistics;
}

std::shared_ptr<Schema> Database::find_table_schema(const std::filesystem::path &name)
{
    return catalog->find_table_schema(name);
}

PageProvider Database::create_page_provider(std::shared_ptr<Schema> schema)
{
    for (const auto &column : *schema) {
        if (column.type == Type::string) {
            return create_flexible_page_provider(std::move(schema));
        }
    }
    return create_fixed_page_provider(std::move(schema));
}


std::shared_ptr<Database> connect(const std::filesystem::path &path, FrameIndex frame_count)
{
    if (!std::filesystem::exists(path)) {
        std::filesystem::create_directories(path);
    }
    return std::make_shared<Database>(path, frame_count);
}

}    // namespace shdb
