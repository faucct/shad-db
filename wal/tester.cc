
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include <atomic>
#include <random>

#include <string>

#include "database.h"

void error_exit() {
  fprintf(stderr, "Error: %s\n", strerror(errno));
  exit(EXIT_FAILURE);
}

void* mount_shared_memory(const std::string& name) {
  int fd = shm_open(name.c_str(), O_CREAT | O_RDWR, 0777);
  if (fd < 0) {
    error_exit();
  }
  if (ftruncate(fd, 1024) != 0) {
    error_exit();
  }

  void* ret = mmap(0, 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (ret == (void*)MAP_FAILED) {
    error_exit();
  }

  return ret;
}

std::string generate_key(std::default_random_engine& e) {
  std::uniform_int_distribution<> d(0, 99999);
  return std::to_string(d(e));
}

std::string generate_val(std::default_random_engine& e) {
  std::uniform_int_distribution<char> d(0, 255);
  std::uniform_int_distribution<> ds(2, 100);
 
  std::string ret;
  int len = ds(e);
  for (int i = 0; i < len; i++) {
    ret += d(e);
  }

  return ret;
}

void run(int seed, std::atomic<int>* counter, database* db) {
  std::default_random_engine e(seed);
  std::uniform_int_distribution<> d(0, 5);
  while (true) {
    std::string key = generate_key(e);
    if (d(e) == 0) {
      db->erase(key);
    } else {
      std::string value = generate_val(e);
      db->set(key, value);
    }
    ++*counter;
  }
}

void run_std(int seed, int count,
             std::unordered_map<std::string, std::string>* values) {
  std::default_random_engine e(seed);
  std::uniform_int_distribution<> d(0, 5);
  for (int i = 0; i < count; i++) {
    std::string key = generate_key(e);
    if (d(e) == 0) {
      values->erase(key);
    } else {
      std::string value = generate_val(e);
      (*values)[key] = value;
    }
  }
}

int main(int argc, char* argv[]) {
  if (argc < 3) {
    fprintf(stderr, "Usage: tester <seed> <run time, in usec>\n");
    return 1;
  }
  int seed = atoi(argv[1]);
  int runtime = atoi(argv[2]);

  std::random_device rd;
  std::uniform_int_distribution<int> dist(0, 1000000000);
  std::string filename = "/tmp/db" + std::to_string(dist(rd));
  std::string shm_name = "/wal_shared_segment" + std::to_string(dist(rd));

  shm_unlink(shm_name.c_str());
  void* parent_ptr = mount_shared_memory(shm_name);
  std::atomic<int>* parent_counter = new(parent_ptr)std::atomic<int>();

  pid_t child_pid = fork();

  if (child_pid == -1) {
    error_exit();
  }

  if (!child_pid) {
    database db(filename.c_str());
    void* child_ptr = mount_shared_memory(shm_name);
    std::atomic<int>* child_counter = reinterpret_cast<std::atomic<int>*>(child_ptr);
    run(seed, child_counter, &db);
    return 0;
  }

  usleep(runtime);
  
  kill(child_pid, SIGKILL);
  int wstatus;
  waitpid(child_pid, &wstatus, 0);
  
  int num = parent_counter->load();
  shm_unlink(shm_name.c_str());

  // expect throughput of at least 1000 rps.
  if (num < runtime / 1000) {
    fprintf(stderr, "Low throughput, only %d writes in %d usec, or %.2f rps",
        num, runtime, (double) (1e6 * num / runtime));
    return 1;
  }

  std::unordered_map<std::string, std::string> mine;
  std::unordered_map<std::string, std::string> mine1;
  
  // Potentially, one more operation was successfully persisted.
  run_std(seed, num, &mine);
  run_std(seed, num + 1, &mine1);

  database recovered(filename.c_str());

  if (unlink(filename.c_str())) {
    fprintf(stderr, "Unable to delete %s: %s\n", filename.c_str(), strerror(errno));
  }

  if (recovered.values() == mine || recovered.values() == mine1) {
    printf("OK\n");
    return 0;
  } else {
    printf("%d\n", num);
    printf("Mismatch\n");
    return 1;
  }
}
